$(document).ready(function() {
    $('#dataTable').DataTable();
} );

$(document).ready(function() {
    $('#dataTablePendingSend').DataTable();
} );

$(document).ready(function() {
    $('#dataTablePendingReceived').DataTable();
} );

$(document).ready(function() {
    $('#dataTableGranted').DataTable();
} );

$(document).ready(function() {
    $('#dataTableDenied').DataTable();
} );



$(document).ready(function() {
    $('#dataTableHomeFeature').DataTable( {
		retrieve: true,	
        "lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
		"iDisplayLength": 5
    } );
} );


$(document).ready(function() {
    $('#dataTableHomeFeatureAssets').DataTable( {
		retrieve: true,	
        "lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
		"iDisplayLength": 5
    } );
} );

$(document).ready(function() {
    $('#dataTableHomeContribution').DataTable( {
		retrieve: true,	
        "lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
		"iDisplayLength": 5
    } );
} );

$(document).ready(function() {
    $('#dataTableHomeContributionFeature').DataTable( {
		retrieve: true,	
        "lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
		"iDisplayLength": 5
    } );
} );

$(document).ready(function() {
    $('#dataTableHomeVD').DataTable( {
		retrieve: true,	
        "lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
		"iDisplayLength": 5
    } );
} );

$(document).ready(function() {
    $('#dataTableDashBoardFeature').DataTable( {
		retrieve: true,	
        "lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
		"iDisplayLength": 5
    } );
} );

$(document).ready(function() {
    $('#dataTableDashBoardContributions').DataTable( {
		retrieve: true,	
        "lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
		"iDisplayLength": 5
    } );
} );

$(document).ready(function() {
    $('#dataTableInsightViewDownload').DataTable( {
		retrieve: true,	
        "lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
		"iDisplayLength": 5
    } );
} );

$(document).ready(function() {
    $('#dataTableInsightMRAUserIdFrom').DataTable( {
		retrieve: true,	
        "lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
		"iDisplayLength": 5
    } );
} );

$(document).ready(function() {
    $('#dataTableInsightMRAUserIdTo').DataTable( {
		retrieve: true,	
        "lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
		"iDisplayLength": 5
    } );
} );

$(document).ready(function() {
    $('#dataTableInsightMRAAssetId').DataTable( {
		retrieve: true,	
        "lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
		"iDisplayLength": 5
    } );
} );
