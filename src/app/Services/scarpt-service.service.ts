import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class ScarptServiceService {
  baseUrl = 'http://localhost:8090';
  httpHeaders = new HttpHeaders({'Content-Type': 'application/json'});
  constructor(private http: HttpClient) { }

  Scarpt(homeURL): Observable<any> {
    return this.http.post(this.baseUrl + '/api/getURLs', homeURL, {headers: this.httpHeaders, observe: 'response'});
  }

  onSubmitSubLinks(subLink): Observable<any> {
    return this.http.post(this.baseUrl + '/api/getForms', subLink, {headers: this.httpHeaders, observe: 'response'});
  }

  onSubmitselectedForm(form): Observable<any> {
    return this.http.post(this.baseUrl + '/api/getData', form, {headers: this.httpHeaders, observe: 'response'});
  }

  execute(data): Observable<any> {
    return this.http.post(this.baseUrl + '/api/execute', data, {headers: this.httpHeaders, observe: 'response'});
  }

  executeSuite(data): Observable<any> {
    return this.http.post(this.baseUrl + '/api/execute/suite', data, {headers: this.httpHeaders, observe: 'response'});
  }

}
