import { TestBed } from '@angular/core/testing';

import { ScarptServiceService } from './scarpt-service.service';

describe('ScarptServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ScarptServiceService = TestBed.get(ScarptServiceService);
    expect(service).toBeTruthy();
  });
});
