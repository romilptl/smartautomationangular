import { Component, OnInit, Injector, AfterViewInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray, FormControl } from '@angular/forms';
import { ScarptServiceService } from '../Services/scarpt-service.service';
import { ToastrService } from 'ngx-toastr';
import { TreeNode } from 'primeng/primeng';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})

export class HomeComponent implements OnInit, AfterViewInit {
  formHomeURL: FormGroup;
  formSubURL: FormGroup;
  formSelectForm: FormGroup;
  formTable: FormGroup;
  formTestSuite: FormGroup;
  formSelectSuite: FormGroup;
  formCreateProject: FormGroup;
  formSelectColumns: FormGroup;
  formSession: FormGroup;

  isScarpt: boolean;
  isData: boolean;
  isDataLoaded: boolean;
  isDataCalled: boolean;
  isSelectedSubURL: boolean;
  isSelectedForm: boolean;
  isScraptDataLoaded: boolean;
  isScraptCalled: boolean;
  scrollToTable: boolean;
  isTable: boolean;
  displayCreateProjectDialog: boolean;
  displayCreateSuiteDialog: boolean;
  ismail: boolean;
  isCookies: boolean;

  selectedNode: TreeNode;
  homeURL: string;
  settingPosition: string;
  toasterService: ToastrService;

  treeNodes: TreeNode[];
  subURLs = [];
  subForms = [];
  projectList = [];
  PoosibleIndexs = [];
  TestSuiteFolder: string[] = [];
  lastSuiteData: string[] = [];
  browsers = ['Chrome', 'Firefox'];
  columns = ['Clone', 'Execute At', 'Index', 'Image', 'Execute', 'Tag', 'Object', 'Through', 'Value', 'Wait Time'];
  settingmenu: boolean;
  resDataTree: any;
  colOption;
  showColumns;
  settingOptions;
  settingChanges;

  constructor(private formBuilder: FormBuilder,
              private scarptService: ScarptServiceService,
              injector: Injector) {
   

    this.toasterService = injector.get(ToastrService);
    if (localStorage.getItem('Projects')) {
      let i = 0; for (i = 0; i < JSON.parse(localStorage.getItem('Projects')).length; i++) {
        this.projectList[i] = JSON.parse(localStorage.getItem('Projects'))[i];
      }
    }
  }

  ngOnInit() {
    console.log('ngOnIt');


    this.initData_fromLocalStrorage();
    this.createForms();

    // set test-suites
    this.TestSuiteFolder = JSON.parse(localStorage.getItem('TestSuite'));
    this.formSelectSuite.get('SelectSuite').setValue(this.TestSuiteFolder);

    // this.isScraptCalled = true;
  }

  ngAfterViewInit() {
    this.settingChanges = this.formSession.controls.isImagesEnable.valueChanges;
    this.settingChanges.subscribe(formData => this.onSettingChanges(formData));
  }


  initData_fromLocalStrorage() {

    // set sideBar position
    if (localStorage.getItem('settingPosition')) {
      this.settingPosition = localStorage.getItem('settingPosition');
    } else {
      localStorage.setItem('settingPosition', 'top');
      this.settingPosition = localStorage.getItem('settingPosition');
    }

    // set manage options
    if (localStorage.getItem('manage')) {
      this.settingOptions =  localStorage.getItem('manage');
    } else {
      localStorage.setItem('manage', JSON.stringify({cookies: true, session: true, captureimage: false, isImagesEnable: false}));
      this.settingOptions = localStorage.getItem('manage');
    }

    // set columns to display
    if (localStorage.getItem('selectColumns')) {
      this.showColumns = localStorage.getItem('selectColumns');
    } else {
      localStorage.setItem('selectColumns',
      JSON.stringify({clone: true, executeat: true, index: false, image: true,
        execute: true, tag: true, object: true, through: false, value: true, waittime: true}));
      this.showColumns = localStorage.getItem('selectColumns');
    }

    // load previous project
    if (localStorage.getItem('loadProject')) {
      this.treeNodes = [];
      this.treeNodes = JSON.parse(localStorage.getItem('loadProject'));

      if (localStorage.getItem('previousData')) {
        this.createFormBuider(JSON.parse(localStorage.getItem('previousData')));
        let i = 0; for (i = 0; i < JSON.parse(localStorage.getItem('previousData')).body.data.length; i++) {
          this.PoosibleIndexs.push(i);
        }
      }
    }

  }

  onSettingChanges(formData) {
    console.log(formData);
    if (formData) {
      this.formSession.get('captureimage').setValue(false);
      this.formSelectColumns.get('image').setValue(false);
      localStorage.setItem('selectColumns', JSON.stringify(this.formSelectColumns.value));
      localStorage.setItem('manage', JSON.stringify(this.formSession.value));
      this.formSession.controls.captureimage.disable();
      this.formSelectColumns.controls.image.disable();
    } else {
      this.formSelectColumns.get('image').setValue(true);
      localStorage.setItem('selectColumns', JSON.stringify(this.formSelectColumns.value));
      localStorage.setItem('manage', JSON.stringify(this.formSession.value));
      this.formSession.controls.captureimage.enable();
      this.formSelectColumns.controls.image.enable();
    }
  }

  setSession(formData) {
    this.settingOptions = localStorage.setItem('manage', JSON.stringify(formData));
  }

  settings() {
    this.settingmenu = true;
  }

  setMenuPosition(position) {
    console.log(position);
    this.settingPosition = position;
    localStorage.setItem('settingPosition', position);
  }

  setColumns(formData) {
    this.settingmenu = false;
    console.log(JSON.stringify(formData));
    localStorage.setItem('selectColumns', JSON.stringify(this.formSelectColumns.value));
    this.showColumns = this.formSelectColumns.value;
  }

  // onFind get the links from DOM source
  findLinks(homeURL) {
    this.isDataCalled = false; this.isDataLoaded = false;
    this.treeNodes = [];
    this.isScraptCalled = true;
    localStorage.setItem('homeURL', JSON.stringify(homeURL));

    this.scarptService.Scarpt(localStorage.getItem('homeURL')).subscribe(
      response => {
        this.fillParentNode(response);
      });

    this.TestSuiteFolder = JSON.parse(localStorage.getItem('TestSuite'));
    this.formSelectSuite.get('SelectSuite').setValue(this.TestSuiteFolder);
    this.isScraptDataLoaded = true;
  }

  fillParentNode(files) {
    files.body.urls.forEach(element => {
      const treeNode: TreeNode = {
        label: element.url,
        data: {
          isForm: false,
          isUrl: true
        }
      };
      this.treeNodes.push(treeNode);
    });
    this.isTable = true;
    localStorage.setItem('subURLS', JSON.stringify(files.body.urls));
    this.isScraptDataLoaded = true;
    localStorage.setItem('loadProject', JSON.stringify(this.treeNodes));
  }

  fillChildNode(files, isUrl) {
    const childnodes: TreeNode[] = [];
    files.body.urls.forEach(element => {
      console.log(element.url);
      const treeNode: TreeNode = {
        label: element.url,
        data: {
          isForm: !isUrl,
          isUrl
        }
      };
      childnodes.push(treeNode);
    });
    if (this.selectedNode.parent == null) {
      this.treeNodes.find(f => f.label === this.selectedNode.label).children = childnodes;
    } else {
      this.selectedNode.children = childnodes;
    }
  }

  nodeSelect(event) {
    this.toasterService.info(event.node.label);
    localStorage.setItem('selectedSubLink', JSON.stringify({ url: event.node.label }));
    console.log(event.node.data.isUrl);

    // return for already explored node
    if (this.selectedNode.children != null && this.selectedNode.children.length > 0) { return; }

    // parent node selected
    if (event.node.parent === undefined || event.node.data.isUrl) {

      this.isDataCalled = false; this.isDataLoaded = false;
      console.log('========click on Parent=========');

      if (this.formSelectSuite.get('SelectSuite').value == null) {
        this.displayCreateSuiteDialog = true;
      }

      // get forms from the choosen URL
      this.scarptService.onSubmitSubLinks(localStorage.getItem('selectedSubLink'))
        .subscribe(
          response => {
            console.log('API RES: ' + JSON.stringify(response));
            this.fillChildNode(response, false);
            localStorage.setItem(event.node.label, JSON.stringify(response.body.urls));
            if (event.node.children === undefined) {
              console.log('forms: ' + JSON.stringify(response.body.forms));
            }
          });
    } else { // child node selected
      console.log('========click on Children=========');
      this.scrollToTable = true;
      this.isDataCalled = true;
      this.isData = false;

      console.log(event.node.parent.children);
      const childNodeIndex = event.node.parent.children.indexOf(event.node) + 1;
      console.log(childNodeIndex);
      const settingOptions = JSON.parse(localStorage.getItem('manage'));
      localStorage.setItem('selectedSubLink', JSON.stringify({ url: event.node.parent.label }));
      localStorage.setItem('selectedForm', JSON.stringify({
        url: JSON.parse(localStorage.getItem('selectedSubLink')).url,
        index: childNodeIndex,
        session: settingOptions.session,
        cookies: settingOptions.cookies,
        captureimage: settingOptions.captureimage,
        isImagesEnable: settingOptions.isImagesEnable,
        browser: 'Chrome',
        isSendMail: true,
        email: null
      }));

      // get form elements for choosen form
      this.scarptService.onSubmitselectedForm(localStorage.getItem('selectedForm'))
        .subscribe(
          response => {
            this.isData = true;
            this.resDataTree = response;
            console.log('data.body.data.length: ' + response.body.data.length);
            let i = 0;
            this.PoosibleIndexs = [];
            for (i = 0; i < response.body.data.length; i++) {
              this.PoosibleIndexs.push(i);
            }
            console.log('Data Received: ' + JSON.stringify(response.body.data));
            localStorage.setItem('data', JSON.stringify(response.body.data));
            console.log('Data lenght : ' + response.body.data.length);
            this.toasterService.success(response.body.message);
            localStorage.setItem('previousData', JSON.stringify(response));
            this.createFormBuider(response);
            this.isSelectedForm = true;
            this.isDataLoaded = true;
          });
    }
  }

  // execute test-case
  execute() {
    this.isDataCalled = false;
    this.isDataLoaded = false;

    this.formTable.get('browser').setValue(this.formSelectForm.get('browser').value);
    this.formTable.get('isSendMail').setValue(this.formSelectForm.get('isSendMail').value);
    this.formTable.get('email').setValue(this.formSelectForm.get('email').value);
    this.formTable.get('url').setValue(JSON.parse(localStorage.getItem('selectedForm')).url);

    const unitToSet = this.setExecutionOrder(this.formTable.value);
    unitToSet.sort(function (a, b) {
      return a.executeAt > b.executeAt ? 1 : a.executeAt < b.executeAt ? - 1 : 0;
     });
    console.log('Before Table: ' + JSON.stringify(this.formTable.value));
    console.log('unitToset: ' + JSON.stringify(unitToSet));
    this.formTable.get('units').setValue(unitToSet);

    const formData = this.formTable.value;
    // this.formTable.get('units').value.sort().reverse();
    localStorage.setItem('selectedForm', JSON.stringify(this.formTable.value));
    this.scarptService.execute(formData)
      .subscribe(
        response => {
          this.fillChildNode(response, true);
          console.log('After Table: ' + JSON.stringify(this.formTable.value));
          console.log('response navlinks: ' + JSON.stringify(response));
          localStorage.setItem('data', '' + JSON.stringify(this.formTable.get('units').value));
          this.toasterService.info(response.body.message);
        },
        error => {
          this.toasterService.error(JSON.stringify(error.error.message));
        });
  }

  // permutation-combination for execution steps
  setExecutionOrder(TableRecords) {

    const size = TableRecords.units.length;
    const finalUnits = []; const mapStatus = [];
    let i = 0; let j = 0;

    for (i = 0; i < size; i++) {
      mapStatus.push(false);
    }

    for (i = 0; i < size; i++) {
      for (j = 0; j < size; j++) {
        console.log('for: ' + j + ' element: ' + TableRecords.units[i].executeAt);
        console.log('TableRecords.units[j].executeAt == i ' + TableRecords.units[j].executeAt + ' i = ' + i + '  j = ' + j);
        if (TableRecords.units[i].executeAt == j) {
          console.log('match found: ' + i + ' for ' + TableRecords.units[i].executeAt);
          finalUnits.push(TableRecords.units[j]);
          mapStatus[i] = true;
        }
      }
    }
    return finalUnits;
  }


  // execute test-suite
  async executeSuite() {
    const suiteSize = JSON.parse(localStorage.getItem(this.formSelectSuite.get('SelectSuite').value)).length;

    let i = 0;
    console.log('Execute DATA ' + JSON.parse(localStorage.getItem(this.formSelectSuite.get('SelectSuite').value))[0]);
    this.formTable.get('browser').setValue(this.formSelectForm.get('browser').value);
    this.formTable.get('isSendMail').setValue(this.formSelectForm.get('isSendMail').value);
    this.formTable.get('email').setValue(this.formSelectForm.get('email').value);

    for (i = 0; i < suiteSize; i++) {
      this.scarptService.execute(JSON.parse(localStorage.getItem(this.formSelectSuite.get('SelectSuite').value))[i])
        .subscribe(
          response => {
            this.toasterService.info(response.body.message);
          },
          error => {
            this.toasterService.error(JSON.stringify(error.error.message));
          });
      await this.delay(18000);
    }
  }

  // Clone Element
  cloneElement(unitToClone) {
    const newElement = unitToClone;
    const control = this.formTable.get('units') as FormArray;

    newElement.executeAt = control.length;
    newElement.elementindex = control.length;
    newElement.isexecute = true;
    newElement.operation = unitToClone.operation;
    newElement.tag = unitToClone.tag;
    newElement.object = unitToClone.object;
    newElement.val = unitToClone.val;

    try {
      (this.formTable.get('units') as FormArray).push(new FormControl(newElement));
    } catch (error) { }

    const testFormGrooup = this.formTable.get('units').value.map(m => this.formBuilder.group(m));
    const testFormArray = this.formBuilder.array(testFormGrooup);
    this.formTable.setControl('units', testFormArray);

    this.PoosibleIndexs.push(control.length - 1);
  }

  createNewProject(newProject) {
    newProject.baseURL = this.formHomeURL.get('url').value;
    this.projectList.push(newProject);
    localStorage.setItem('currentProject', JSON.stringify(newProject));
    localStorage.setItem('Projects', JSON.stringify(this.projectList));
    this.displayCreateProjectDialog = false;
  }

  // Test-Suite Operations
  AddtoSuite() {
    // console.log('Data to Set: ' + JSON.stringify(this.formTable.value))
    console.log('suite: ' + this.formSelectSuite.get('SelectSuite').value);
    this.lastSuiteData = JSON.parse(localStorage.getItem(this.formSelectSuite.get('SelectSuite').value));

    if (this.lastSuiteData != null) {
      this.formTable.get('url').setValue(JSON.parse(localStorage.getItem('selectedForm')).url);
      this.lastSuiteData.push(this.formTable.value);
      localStorage.setItem(this.formSelectSuite.get('SelectSuite').value, JSON.stringify(this.lastSuiteData));
    } else {
      this.formTable.get('url').setValue(JSON.parse(localStorage.getItem('selectedForm')).url);
      this.lastSuiteData = [];
      this.lastSuiteData.push(JSON.parse(JSON.stringify(this.formTable.value)));
      localStorage.setItem(this.formSelectSuite.get('SelectSuite').value, JSON.stringify(this.lastSuiteData));
    }
  }

  onNewTestSuite(suitename) {
    console.log('New TestSuite: ' + this.TestSuiteFolder);
    if (this.TestSuiteFolder == null) {
      this.TestSuiteFolder = [];
      this.TestSuiteFolder.push(suitename);
      console.log('If');
    } else {
      this.TestSuiteFolder.push(suitename);
    }

    console.log('Added: ' + this.TestSuiteFolder);
    localStorage.setItem('TestSuite', JSON.stringify(this.TestSuiteFolder));

    this.displayCreateSuiteDialog = false;
  }

  // Dyanmic Form Opertions
  createFormBuider(response) {
    this.resDataTree = response;
    let i = 0;
    for (i = 0; i < response.body.data.length; i++) {
      this.resDataTree.body.data[i].image = 'data:image/png;base64,' + response.body.data[i].image;
    }
    const testFormGrooup = response.body.data.map(m => this.formBuilder.group(m));
    const testFormArray = this.formBuilder.array(testFormGrooup);
    console.log('testFormArray', testFormArray);
    console.log("form" + this.formTable);
    if(this.formTable == undefined) {
      this.createForm_Table();
    }
    this.formTable.setControl('units', testFormArray);
  }

  get unnitsFormArray(): FormArray {
    return this.formTable.get('units') as FormArray;
  }

  // TreeNode expand, collapse
  private expandRecursive(node: TreeNode, isExpand: boolean) {
    console.log('Expand Call: ' + node.expanded);
    node.expanded = isExpand;
    if (node.children) {
      console.log('node.childern: ' + node.children);
      node.children.forEach(childNode => {
        this.expandRecursive(childNode, isExpand);
      });
    }
  }

  expandAll() {
    this.treeNodes.forEach(node => {
      this.expandRecursive(node, true);
    });
  }

  collapseAll() {
    this.treeNodes.forEach(node => {
      this.expandRecursive(node, false);
    });
  }

  nodeUnselect(event) { console.log('unselected node: ' + event); }

  // Jump to Table
  scroll(el: HTMLElement) {
    console.log('Target Called' + el);
    // if(this.scrollToTable)
    //   el.scrollIntoView();
  }

  // Delay for Test Suite Execution
  delay(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  // Show Dialog Modals
  showCreateProkectDialog() {
    this.displayCreateProjectDialog = true;
  }

  // Create Forms
  createForms() {
    console.log('create forms');
    this.createForm_HomeURL();
    this.createForm_SubURL();
    this.createForm_CreateProject();
    this.createForm_SelectForm();
    this.createForm_Table();
    this.createForm_CreateTestSuite();
    this.createForm_SelectSuite();
    this.createForm_SelectColumns();
    this.createForm_Session_Coookies();
  }

  createForm_Session_Coookies() {
    this.formSession = this.formBuilder.group({
      cookies: [],
      session: [],
      captureimage: [],
      isImagesEnable: []
    });
    return this.formSession;
  }

  createForm_HomeURL() {
    this.formHomeURL = this.formBuilder.group({
      url: ['', [Validators.required, Validators.minLength(10)]],
    });
    return this.formHomeURL;
  }

  createForm_SubURL() {
    this.formSubURL = this.formBuilder.group({
      url: ['Select Link']
    });
    return this.formSubURL;
  }

  createForm_CreateProject() {
    this.formCreateProject = this.formBuilder.group({
      newProject: [],
      baseURL: []
    });
    return this.formCreateProject;
  }

  createForm_SelectForm() {
    this.formSelectForm = this.formBuilder.group({
      url: ['Select Form'],
      index: [],
      browser: ['Select Browser', [Validators.required]],
      isSendMail: [this.ismail],
      email: []
    });
    return this.formSelectForm;
  }

  createForm_Table() {
    this.formTable = this.formBuilder.group({
      units: this.formBuilder.array([]),
      browser: [, [Validators.required]],
      isSendMail: [this.ismail],
      email: [],
      url: []
    });
    return this.formTable;
  }

  createForm_CreateTestSuite() {
    this.formTestSuite = this.formBuilder.group({
      suite: []
    });
    return this.formTestSuite;
  }

  createForm_SelectSuite() {
    this.formSelectSuite = this.formBuilder.group({
      SelectSuite: ['Select Test Suite']
    });
    return this.formSelectSuite;
  }

  createForm_SelectColumns() {
    this.formSelectColumns = this.formBuilder.group({
      clone: [JSON.parse(this.showColumns).clone],
      executeat: [JSON.parse(this.showColumns).executeat],
      index: [JSON.parse(this.showColumns).index],
      image: [JSON.parse(this.showColumns).image],
      execute: [JSON.parse(this.showColumns).execute],
      tag: [JSON.parse(this.showColumns).tag],
      object: [JSON.parse(this.showColumns).object],
      through: [JSON.parse(this.showColumns).through],
      value: [JSON.parse(this.showColumns).value],
      waittime: [JSON.parse(this.showColumns).waittime],
    });
    this.showColumns = this.formSelectColumns.value;
    return this.formSelectColumns;
  }

  // Mail Operation
  isMailChange() {
    if (this.ismail === false) {
      this.ismail = true;
      this.formTable.get('isSendMail').setValue(this.ismail);
      this.formSelectForm.get('isSendMail').setValue(this.ismail);
    } else {
      this.ismail = false;
      this.formTable.get('isSendMail').setValue(this.ismail);
      this.formSelectForm.get('isSendMail').setValue(this.ismail);
    }
  }

}
