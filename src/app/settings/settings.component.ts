import { Component, OnInit } from '@angular/core';
import { SelectItem } from 'primeng/primeng';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css']
})
export class SettingsComponent implements OnInit {
  selectColumns: FormGroup;
  types: SelectItem[];
  // selectedTypes: string[] = ['Clone', 'Execute At', 'Index', 'Image', 'Execute', 'Tag', 'Object', 'Through', 'Value'];

  constructor(private formBuilder: FormBuilder) {
    this.types = [
      {label: 'Clone', value: { index: '1', value: 'Clone', enable: true } },
      {label: 'Execute At', value: { index: '2', value: 'Execute At' } },
      {label: 'Index', value: { index: '3', value: 'Index' } },
      {label: 'Image', value: { index: '4', value: 'Image' } },
      {label: 'Execute', value: { index: '5', value: 'Execute' } },
      {label: 'Tag', value: { index: '6', value: 'Tag' } },
      {label: 'Object', value: { index: '7', value: 'Object' } },
      {label: 'Through', value: { index: '8', value: 'Through' } },
      {label: 'Value', value: { index: '9', value: 'Value' } },
  ];
  }

  ngOnInit() {
    this.selectColumns = this.formBuilder.group({
      columns: [],
    });
  }


  saveCol() {
    console.log(JSON.stringify(this.selectColumns.value));
    localStorage.setItem('selectColumns', JSON.stringify(this.selectColumns.value));
  }
}
